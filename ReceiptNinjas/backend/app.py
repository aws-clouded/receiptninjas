# Backend server code (e.g., Flask, Django or any other preference)

from datasets import load_dataset
from ast import literal_eval

dataset = load_dataset("naver-clova-ix/cord-v2")


example = dataset['train'][0]
image = example['image']
# let's make the image a bit smaller when visualizing
ground_truth = example['ground_truth']

print(literal_eval(ground_truth)['gt_parse'])
